import React, { Component } from 'react';
import './App.css';
import Panel from "./Panel/Panel";
import Fields from "./Fields/Fields";
import axios from 'axios';


class App extends Component {

    state = {
        message: '',
        author: '',
        getmessage: {}
    };

    GetMessage = () => {
        axios.get('/message.json').then(response => {

            this.setState({getmessage:response.data});
        });
    };


    componentDidMount() {
        this.GetMessage()
    };

    fullchange = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    addMessage = () => {
        axios.post('/message.json', { addmessage:this.state.message, author: this.state.author, date: new Date()}).then(() => {
            this.GetMessage();
            }
        )
    };


  render() {
    return (
      <div className="App">
        <Panel  change = {this.fullchange} send = {this.addMessage } message = {this.state.message} author={this.state.author}/>
        <Fields  messages = {this.state.getmessage} />

      </div>
    );
  }
}

export default App;


