import React from 'react';
import './Fields.css'
import Field from "../Field/Field";

let Fields = (props)=> {
    let messages = Object.keys(props.messages);
    return(
        <div className='fields'>
            {messages.map((messageId) => <Field text={props.messages[messageId].addmessage} author={props.messages[messageId].author}  date={props.messages[messageId].date}  />)}
        </div>
    )
};

export default Fields;