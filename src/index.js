import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
axios.defaults.baseURL = 'https://vik2405-ed905.firebaseio.com';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
