import React from 'react';
import './Field.css'

let Field = (props)=> {

    return(
        <div className='field'>
            <h3 id="field">{props.text} {props.date} {props.author}</h3>
        </div>
    )
};

export default Field;